package rf.androidovshchik.simplevideoaudio.data;

import com.devbrackets.android.playlistcore.annotation.SupportedMediaType;
import com.devbrackets.android.playlistcore.api.PlaylistItem;

import rf.androidovshchik.simplevideoaudio.manager.PlaylistManager;

/**
 * A custom {@link PlaylistItem}
 * to hold the information pertaining to the audio and video items
 */
public class MediaItem implements PlaylistItem {

    private Sample sample;
    boolean isAudio;

    public MediaItem(Sample sample, boolean isAudio) {
        this.sample = sample;
        this.isAudio = isAudio;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public boolean getDownloaded() {
        return false;
    }

    @Override
    @SupportedMediaType
    public int getMediaType() {
        return isAudio ? PlaylistManager.AUDIO : PlaylistManager.VIDEO;
    }

    @Override
    public String getMediaUrl() {
        return sample.getMediaUrl();
    }

    @Override
    public String getDownloadedMediaUri() {
        return null;
    }

    @Override
    public String getThumbnailUrl() {
        return sample.getArtworkUrl();
    }

    @Override
    public String getArtworkUrl() {
        return sample.getArtworkUrl();
    }

    @Override
    public String getTitle() {
        return sample.getTitle();
    }

    @Override
    public String getAlbum() {
        return "Unknown album";
    }

    @Override
    public String getArtist() {
        return "Unknown artist";
    }
}