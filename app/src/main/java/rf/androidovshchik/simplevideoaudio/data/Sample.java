package rf.androidovshchik.simplevideoaudio.data;

import android.support.annotation.Nullable;

public class Sample {

    private String title;

    private String mediaUrl;

    private String artworkUrl;

    public Sample(String title, String mediaUrl) {
        this(title, mediaUrl, null);
    }

    public Sample(String title, String mediaUrl, String artworkUrl) {
        this.title = title;
        this.mediaUrl = mediaUrl;
        this.artworkUrl = artworkUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    @Nullable
    public String getArtworkUrl() {
        return artworkUrl;
    }
}
