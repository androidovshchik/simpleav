package rf.androidovshchik.simplevideoaudio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.video)
    void onVideo() {
        startActivity(new Intent(getApplicationContext(), VideoActivity.class));
    }

    @OnClick(R.id.audio)
    void onAudio() {
        startActivity(new Intent(getApplicationContext(), AudioActivity.class));
    }
}
