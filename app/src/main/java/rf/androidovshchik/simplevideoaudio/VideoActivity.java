package rf.androidovshchik.simplevideoaudio;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.devbrackets.android.exomedia.listener.VideoControlsSeekListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

import java.util.LinkedList;
import java.util.List;

import rf.androidovshchik.simplevideoaudio.data.MediaItem;
import rf.androidovshchik.simplevideoaudio.data.Sample;
import rf.androidovshchik.simplevideoaudio.manager.PlaylistManager;
import rf.androidovshchik.simplevideoaudio.playlist.VideoApi;
import timber.log.Timber;

public class VideoActivity extends AppCompatActivity implements VideoControlsSeekListener {

    public static final int PLAYLIST_ID = 6; //Arbitrary, for the example (different from audio)

    protected VideoApi videoApi;
    protected VideoView videoView;
    protected PlaylistManager playlistManager;

    private long position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_activity);
        videoView = findViewById(R.id.video_play_activity_video_view);
        videoView.setHandleAudioFocus(false);
        videoView.getVideoControls().setSeekListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Timber.d("onStart position %d", position);
        setupPlaylistManager();
        videoApi = new VideoApi(videoView);
        playlistManager.addVideoApi(videoApi);
        playlistManager.play(position, position > 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        position = videoView.getCurrentPosition();
        Timber.d("onStop position %d", position);
        playlistManager.removeVideoApi(videoApi);
        playlistManager.invokeStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playlistManager.invokeStop();
    }

    @Override
    public boolean onSeekStarted() {
        playlistManager.invokeSeekStarted();
        return true;
    }

    @Override
    public boolean onSeekEnded(long seekTime) {
        playlistManager.invokeSeekEnded(seekTime);
        return true;
    }

    /**
     * Retrieves the playlist instance and performs any generation
     * of content if it hasn't already been performed.
     */
    private void setupPlaylistManager() {
        playlistManager = ((MainApplication)getApplicationContext()).getPlaylistManager();
        List<MediaItem> mediaItems = new LinkedList<>();
        MediaItem mediaItem = new MediaItem(new Sample("Untitled",
            getString(R.string.video_link)), false);
        mediaItems.add(mediaItem);
        playlistManager.setParameters(mediaItems, 0);
        playlistManager.setId(PLAYLIST_ID);
    }
}
