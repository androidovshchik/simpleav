package rf.androidovshchik.simplevideoaudio.service;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.devbrackets.android.playlistcore.api.MediaPlayerApi;
import com.devbrackets.android.playlistcore.components.image.ImageProvider;
import com.devbrackets.android.playlistcore.components.playlisthandler.DefaultPlaylistHandler;
import com.devbrackets.android.playlistcore.components.playlisthandler.PlaylistHandler;
import com.devbrackets.android.playlistcore.service.BasePlaylistService;

import org.jetbrains.annotations.Nullable;

import rf.androidovshchik.simplevideoaudio.MainApplication;
import rf.androidovshchik.simplevideoaudio.R;
import rf.androidovshchik.simplevideoaudio.data.MediaItem;
import rf.androidovshchik.simplevideoaudio.manager.PlaylistManager;
import rf.androidovshchik.simplevideoaudio.playlist.AudioApi;

/**
 * A simple service that extends {@link BasePlaylistService} in order to provide
 * the application specific information required.
 */
public class MediaService extends BasePlaylistService<MediaItem, PlaylistManager> {

    @Override
    public void onCreate() {
        super.onCreate();

        // Adds the audio player implementation, otherwise there's nothing to play media with
        getPlaylistManager().getMediaPlayers().add(new AudioApi(getApplicationContext()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Releases and clears all the MediaPlayersMediaImageProvider
        for (MediaPlayerApi<MediaItem> player : getPlaylistManager().getMediaPlayers()) {
            player.release();
        }

        getPlaylistManager().getMediaPlayers().clear();
    }

    @NonNull
    @Override
    protected PlaylistManager getPlaylistManager() {
        return ((MainApplication)getApplicationContext()).getPlaylistManager();
    }

    @NonNull
    @Override
    public PlaylistHandler<MediaItem> newPlaylistHandler() {
        return new DefaultPlaylistHandler.Builder<>(
            getApplicationContext(),
            getClass(),
            getPlaylistManager(),
            new ImageProvider<MediaItem>() {
                @Override
                public int getNotificationIconRes() {
                    return R.drawable.ic_music_note_white_24dp;
                }

                @Override
                public int getRemoteViewIconRes() {
                    return 0;
                }

                @Nullable
                @Override
                public Bitmap getLargeNotificationImage() {
                    return null;
                }

                @Nullable
                @Override
                public Bitmap getRemoteViewArtwork() {
                    return null;
                }

                @Override
                public void updateImages(MediaItem mediaItem) {
                    getPlaylistHandler().updateMediaControls();
                }
            }
        ).build();
    }
}